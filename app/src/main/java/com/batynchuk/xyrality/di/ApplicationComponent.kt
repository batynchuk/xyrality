package com.batynchuk.xyrality.di

import android.content.Context
import com.batynchuk.xyrality.XyralityApplication
import com.batynchuk.xyrality.common.rest.RestService
import com.batynchuk.xyrality.data.BaseLoginRepository
import com.batynchuk.xyrality.data.BaseWorldRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class)])
interface ApplicationComponent {

    fun inject(application: XyralityApplication)

    @ApplicationContext
    fun applicationContext(): Context

    fun worldRepository(): BaseWorldRepository

    fun loginRepository(): BaseLoginRepository

    fun restServiceProvide(): RestService
}