package com.batynchuk.xyrality.di

import android.content.Context
import com.batynchuk.xyrality.BuildConfig
import com.batynchuk.xyrality.XyralityApplication
import com.batynchuk.xyrality.common.rest.RestService
import com.batynchuk.xyrality.data.BaseLoginRepository
import com.batynchuk.xyrality.data.BaseWorldRepository
import com.batynchuk.xyrality.data.remote.login.RemoteLoginRepository
import com.batynchuk.xyrality.data.remote.world.RemoteWorldRepository
import com.batynchuk.xyrality.data.remote.world.WorldService
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: XyralityApplication) {

    @Provides
    fun providesApplication(): XyralityApplication = application

    @Provides
    @ApplicationContext
    fun providesApplicationContext(): Context = application

    @Provides
    @Singleton
    fun providesGsonBuilder() = GsonBuilder()

    @Provides
    @Singleton
    fun providesServerUrl() = BuildConfig.SERVER_URL

    @Provides
    @Singleton
    fun provideRestService(gsonBuilder: GsonBuilder) = RestService(gsonBuilder)

    @Provides
    @Singleton
    fun providesWorldsRepository(restService: RestService, serverUrl: String): BaseWorldRepository
            = RemoteWorldRepository(restService.create(WorldService::class.java, serverUrl))

    @Provides
    @Singleton
    fun providesLoginRepository(): BaseLoginRepository = RemoteLoginRepository()

}