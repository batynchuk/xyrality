package com.batynchuk.xyrality.presentation.world.di

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.batynchuk.xyrality.data.BaseWorldRepository
import com.batynchuk.xyrality.di.ActivityScope
import com.batynchuk.xyrality.presentation.world.WorldsViewModelFactory
import com.batynchuk.xyrality.presentation.world.WorldsActivity
import com.batynchuk.xyrality.presentation.world.WorldsPresenter
import com.batynchuk.xyrality.presentation.world.domain.WorldsInteractor
import com.batynchuk.xyrality.presentation.world.domain.WorldsMapperInteractor
import dagger.Module
import dagger.Provides

@Module
class WorldsModule(private val worldsActivity: WorldsActivity) {

    @Provides
    @ActivityScope
    fun providesLayoutManager(): RecyclerView.LayoutManager {
        return LinearLayoutManager(worldsActivity, LinearLayoutManager.VERTICAL, false)
    }

    @Provides
    @ActivityScope
    fun provideWorldsInteractor(repository: BaseWorldRepository): WorldsInteractor
            = WorldsMapperInteractor(repository)

    @Provides
    @ActivityScope
    fun provideModelFactory(interactor: WorldsInteractor) : WorldsViewModelFactory = WorldsViewModelFactory(interactor)

    @Provides
    @ActivityScope
    fun provideWorldsPresenter(worldsViewModelFactory: WorldsViewModelFactory): WorldsPresenter
            = ViewModelProviders.of(worldsActivity, worldsViewModelFactory).get(WorldsPresenter::class.java)

}