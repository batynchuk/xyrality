package com.batynchuk.xyrality.presentation.world

import com.batynchuk.xyrality.common.base.MVPView
import com.batynchuk.xyrality.presentation.world.domain.entity.World

interface WorldsView : MVPView {

    fun showLoadingView()

    fun hideLoadingView()

    fun showWorlds(worlds: List<World>?)

    fun showError()
}