package com.batynchuk.xyrality.presentation.login.di

import com.batynchuk.xyrality.di.ActivityScope
import com.batynchuk.xyrality.di.ApplicationComponent
import com.batynchuk.xyrality.presentation.login.LoginActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [(ApplicationComponent::class)], modules = [(LoginModule::class)])
interface LoginComponent {
    fun inject(loginActivity: LoginActivity)
}