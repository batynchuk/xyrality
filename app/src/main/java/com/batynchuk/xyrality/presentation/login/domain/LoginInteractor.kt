package com.batynchuk.xyrality.presentation.login.domain

import com.batynchuk.xyrality.common.base.Interactor
import com.batynchuk.xyrality.presentation.login.domain.entity.Login

interface LoginInteractor : Interactor {

    fun login(login: String,
              password: CharSequence,
              onNext: (login : Login) -> Unit = {},
              onError: (Throwable) -> Unit = {})
}