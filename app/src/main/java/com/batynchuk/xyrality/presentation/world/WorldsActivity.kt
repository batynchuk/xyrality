package com.batynchuk.xyrality.presentation.world

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import com.batynchuk.xyrality.R
import com.batynchuk.xyrality.XyralityApplication
import com.batynchuk.xyrality.presentation.world.adapter.WorldsAdapter
import com.batynchuk.xyrality.presentation.world.di.DaggerWorldsComponent
import com.batynchuk.xyrality.presentation.world.di.WorldsModule
import com.batynchuk.xyrality.presentation.world.domain.entity.World
import kotlinx.android.synthetic.main.activity_worlds.*
import javax.inject.Inject

class WorldsActivity : AppCompatActivity(), WorldsView {

    private val component by lazy {
        DaggerWorldsComponent.builder()
                .worldsModule(WorldsModule(this))
                .applicationComponent((application as XyralityApplication).component)
                .build()
    }

    private val worldsAdapter = WorldsAdapter()

    @Inject
    lateinit var presenter: WorldsPresenter

    @Inject
    lateinit var layoutManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_worlds)

        component.inject(this)
        presenter.onAttachView(this)
        presenter.getWorldList().observe(this, Observer {
            showWorlds(it)
        })

        rvWorlds.layoutManager = layoutManager
        rvWorlds.adapter = worldsAdapter

        presenter.handleBundle(intent.extras)

    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetachView()
    }

    override fun showLoadingView() {
        pbLoading.visibility = View.VISIBLE
    }

    override fun hideLoadingView() {
        pbLoading.visibility = View.GONE
    }

    override fun showWorlds(worlds: List<World>?) {
        rvWorlds.visibility = View.VISIBLE
        worlds?.let { worldsAdapter.showWorlds(worlds) }

    }

    override fun showError() {
        Toast.makeText(this, R.string.error_occurred, Toast.LENGTH_SHORT).show()
    }

}
