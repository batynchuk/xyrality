package com.batynchuk.xyrality.presentation.world.domain.entity

import com.batynchuk.xyrality.data.entity.world.WorldEntity

fun WorldEntity.mapToWorld(): World {
    return World(this.id,
            this.language,
            this.url,
            this.country,
            WorldStatus(this.worldStatusEntity?.id, this.worldStatusEntity?.description),
            this.mapUrl,
            this.name)

}