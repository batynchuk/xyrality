package com.batynchuk.xyrality.presentation.world.domain.entity

data class World(var id: Long?,
                 var language: String?,
                 var url: String?,
                 var country: String?,
                 var worldStatus: WorldStatus?,
                 var mapUrl: String?,
                 var name: String?)