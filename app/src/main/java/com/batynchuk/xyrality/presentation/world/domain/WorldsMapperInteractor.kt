package com.batynchuk.xyrality.presentation.world.domain

import com.batynchuk.xyrality.common.base.BaseInteractor
import com.batynchuk.xyrality.common.util.getMACAddress
import com.batynchuk.xyrality.data.BaseWorldRepository
import com.batynchuk.xyrality.data.entity.world.WorldResponseEntity
import com.batynchuk.xyrality.presentation.world.domain.entity.World
import com.batynchuk.xyrality.presentation.world.domain.entity.mapToWorld
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class WorldsMapperInteractor(private val repository: BaseWorldRepository) : BaseInteractor(), WorldsInteractor {

    private val deviceType = String.format("%s %s", android.os.Build.MODEL, android.os.Build.VERSION.RELEASE)
    private val deviceId = getMACAddress("Ethernet")

    private var isLoading = false

    override fun loadWorlds(login: String, password: CharSequence, onNext: (List<World>) -> Unit, onError: (Throwable) -> Unit) {
        processWorlds(repository.getWorlds(login, password, deviceType, deviceId), onNext, onError)
    }

    private fun processWorlds(worlds: Observable<WorldResponseEntity>, onNext: (List<World>) -> Unit, onError: (Throwable) -> Unit) {
        isLoading = true

        disposables.add(worlds.doOnTerminate { isLoading = false }
                .map { it.allAvailableWorlds }
                .map { it.map { it.mapToWorld() } }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onNext, onError)
        )
    }
}