package com.batynchuk.xyrality.presentation.login.domain.entity

data class Login(val login: String, val password: CharSequence)