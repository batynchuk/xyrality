package com.batynchuk.xyrality.presentation.world.domain.entity

data class WorldStatus(var id: Int?,
                       var description: String?)