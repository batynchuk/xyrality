package com.batynchuk.xyrality.presentation.world

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.batynchuk.xyrality.presentation.world.domain.WorldsInteractor

class WorldsViewModelFactory(private val interactor: WorldsInteractor) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WorldsPresenter(interactor) as T
    }

}