package com.batynchuk.xyrality.presentation.login.di

import com.batynchuk.xyrality.data.BaseLoginRepository
import com.batynchuk.xyrality.di.ActivityScope
import com.batynchuk.xyrality.presentation.login.LoginActivity
import com.batynchuk.xyrality.presentation.login.LoginPresenter
import com.batynchuk.xyrality.presentation.login.domain.LoginInteractor
import com.batynchuk.xyrality.presentation.login.domain.LoginMapperInteractor
import dagger.Module
import dagger.Provides

@Module
class LoginModule(private val loginActivity: LoginActivity) {

    @Provides
    @ActivityScope
    fun provideLoginInteractor(repository: BaseLoginRepository): LoginInteractor
            = LoginMapperInteractor(repository)

    @Provides
    @ActivityScope
    fun provideLoginPresenter(interactor: LoginInteractor): LoginPresenter
            = LoginPresenter(interactor)
}