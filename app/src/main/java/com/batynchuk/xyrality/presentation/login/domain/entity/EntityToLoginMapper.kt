package com.batynchuk.xyrality.presentation.login.domain.entity

import com.batynchuk.xyrality.data.entity.login.LoginEntity

fun LoginEntity.mapToLogin(): Login {
    return Login(login, password)
}