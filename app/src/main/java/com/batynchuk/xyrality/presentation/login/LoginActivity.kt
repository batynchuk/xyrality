package com.batynchuk.xyrality.presentation.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.batynchuk.xyrality.R
import com.batynchuk.xyrality.XyralityApplication
import com.batynchuk.xyrality.presentation.login.di.DaggerLoginComponent
import com.batynchuk.xyrality.presentation.login.di.LoginModule
import com.batynchuk.xyrality.presentation.world.LOGIN_KEY
import com.batynchuk.xyrality.presentation.world.PASSWORD_KEY
import com.batynchuk.xyrality.presentation.world.WorldsActivity
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject


class LoginActivity : AppCompatActivity(), LoginView {

    private val component by lazy {
        DaggerLoginComponent.builder()
                .loginModule(LoginModule(this))
                .applicationComponent((application as XyralityApplication).component)
                .build()
    }

    @Inject
    lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        component.inject(this)
        presenter.onAttachView(this)

        btnSubmit?.setOnClickListener { presenter.login(etLogin?.text.toString(), etPassword?.text) }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetachView()
    }

    override fun showEmptyInput() {
        Toast.makeText(this, R.string.empty_input, Toast.LENGTH_SHORT).show()
    }

    override fun showLoginError() {
        Toast.makeText(this, R.string.login_error, Toast.LENGTH_SHORT).show()
    }

    override fun openWorldsActivity(login: String, password: CharSequence) {
        val intent = Intent(this, WorldsActivity::class.java)
        intent.putExtra(LOGIN_KEY, login)
        intent.putExtra(PASSWORD_KEY, password)
        startActivity(intent)
    }

    override fun showLoadingView() {
        pbLoading.visibility = View.VISIBLE
    }

    override fun hideLoadingView() {
        pbLoading.visibility = View.GONE
    }

}
