package com.batynchuk.xyrality.presentation.world.di

import com.batynchuk.xyrality.di.ActivityScope
import com.batynchuk.xyrality.di.ApplicationComponent
import com.batynchuk.xyrality.presentation.world.WorldsActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [(ApplicationComponent::class)], modules = [(WorldsModule::class)])
interface WorldsComponent {
    fun inject(worldsActivity: WorldsActivity)
}