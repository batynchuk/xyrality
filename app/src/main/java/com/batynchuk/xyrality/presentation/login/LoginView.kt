package com.batynchuk.xyrality.presentation.login

import com.batynchuk.xyrality.common.base.MVPView

interface LoginView : MVPView {

    fun showEmptyInput()

    fun showLoginError()

    fun openWorldsActivity(login: String, password: CharSequence)

    fun showLoadingView()

    fun hideLoadingView()

}