package com.batynchuk.xyrality.presentation.login

import com.batynchuk.xyrality.common.base.BasePresenter
import com.batynchuk.xyrality.common.util.safeLet
import com.batynchuk.xyrality.presentation.login.domain.LoginInteractor
import com.batynchuk.xyrality.presentation.login.domain.entity.Login

class LoginPresenter(private val loginInteractor: LoginInteractor) : BasePresenter<LoginView>() {

    fun login(login: String?, password: CharSequence?) {
        safeLet(login, password) { login, password ->
            run {
                if (login.isNotEmpty() && password.isNotEmpty()) {
                    loginInteractor.login(login, password, { onNext(it) }, { onError(it) })
                } else{
                    mvpView?.showEmptyInput()
                }
            }
        }
    }

    private fun onNext(login: Login) {
        mvpView?.hideLoadingView()
        mvpView?.openWorldsActivity(login.login, login.password)
    }

    private fun onError(throwable: Throwable) {
        mvpView?.hideLoadingView()
        mvpView?.showLoginError()
    }
}