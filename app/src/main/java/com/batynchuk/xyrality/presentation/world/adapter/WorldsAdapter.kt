package com.batynchuk.xyrality.presentation.world.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.batynchuk.xyrality.R
import com.batynchuk.xyrality.presentation.world.domain.entity.World
import kotlinx.android.synthetic.main.item_world.view.*

class WorldsAdapter : RecyclerView.Adapter<WorldsAdapter.WorldViewHolder>() {

    private val items = mutableListOf<World>()
    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorldViewHolder {
        context = parent.context
        return WorldViewHolder(LayoutInflater.from(context).inflate(R.layout.item_world, parent, false))
    }

    override fun onBindViewHolder(holder: WorldViewHolder, position: Int) {
        val world = items[position]
        holder.itemView.tvName.text = world.name
        holder.itemView.tvCountry.text =
                String.format("%s: %s", context?.getString(R.string.country), world.country)
        holder.itemView.tvStatus.text =
                String.format("%s: %s", context?.getString(R.string.status), world.worldStatus?.description)
    }

    override fun getItemCount() = items.size

    fun showWorlds(list: List<World>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    class WorldViewHolder(view: View) : RecyclerView.ViewHolder(view)
}