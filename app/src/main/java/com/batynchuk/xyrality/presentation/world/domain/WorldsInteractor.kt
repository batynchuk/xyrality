package com.batynchuk.xyrality.presentation.world.domain

import com.batynchuk.xyrality.common.base.Interactor
import com.batynchuk.xyrality.presentation.world.domain.entity.World

interface WorldsInteractor : Interactor {

    fun loadWorlds(login: String,
                   password: CharSequence,
                   onNext: (List<World>) -> Unit = {},
                   onError: (Throwable) -> Unit = {})
}
