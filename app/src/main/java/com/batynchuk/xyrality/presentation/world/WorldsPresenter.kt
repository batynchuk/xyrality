package com.batynchuk.xyrality.presentation.world

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.os.Bundle
import com.batynchuk.xyrality.common.base.BasePresenter
import com.batynchuk.xyrality.common.util.safeLet
import com.batynchuk.xyrality.presentation.world.domain.WorldsInteractor
import com.batynchuk.xyrality.presentation.world.domain.entity.World

const val LOGIN_KEY = "login"
const val PASSWORD_KEY = "password"

class WorldsPresenter(private val worldsInteractor: WorldsInteractor) : BasePresenter<WorldsView>() {

    private var worldsList = MutableLiveData<List<World>>()

    fun handleBundle(bundle: Bundle) {
        if (worldsList.value == null) {
            val login = bundle.getString(LOGIN_KEY)
            val password = bundle.getCharSequence(PASSWORD_KEY)
            loadWorlds(login, password)
        }
    }

    fun loadWorlds(login: String, password: CharSequence) {
        mvpView?.showLoadingView()
        worldsInteractor.loadWorlds(login, password, { onNext(it) }, { onError(it) })
    }

    fun getWorldList(): LiveData<List<World>> = worldsList

    private fun onNext(list: List<World>) {
        mvpView?.hideLoadingView()
        worldsList.value = list
    }

    private fun onError(throwable: Throwable) {
        mvpView?.hideLoadingView()
        mvpView?.showError()
        throwable.printStackTrace()
    }

    override fun onDetachView() {
        super.onDetachView()
        worldsInteractor.onDetach()
    }
}