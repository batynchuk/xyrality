package com.batynchuk.xyrality.presentation.login.domain

import com.batynchuk.xyrality.common.base.BaseInteractor
import com.batynchuk.xyrality.data.BaseLoginRepository
import com.batynchuk.xyrality.data.entity.login.LoginEntity
import com.batynchuk.xyrality.presentation.login.domain.entity.Login
import com.batynchuk.xyrality.presentation.login.domain.entity.mapToLogin
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginMapperInteractor(private val repository: BaseLoginRepository) : BaseInteractor(), LoginInteractor {

    override fun login(login: String, password: CharSequence, onNext: (login: Login) -> Unit, onError: (Throwable) -> Unit) {
        processLogin(repository.login(login, password), onNext, onError)
    }

    private fun processLogin(observable: Observable<LoginEntity>, onNext: (login: Login) -> Unit, onError: (Throwable) -> Unit) {
        disposables.add(observable
                .map { it.mapToLogin() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onNext, onError))
    }

}