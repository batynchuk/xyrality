package com.batynchuk.xyrality.data

import com.batynchuk.xyrality.data.entity.login.LoginEntity
import com.batynchuk.xyrality.presentation.login.domain.entity.Login
import io.reactivex.Completable
import io.reactivex.Observable

interface BaseLoginRepository{
    fun login(login: String,
              password: CharSequence) : Observable<LoginEntity>
}