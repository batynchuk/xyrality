package com.batynchuk.xyrality.data.remote.world

import com.batynchuk.xyrality.data.entity.world.WorldResponseEntity
import io.reactivex.Observable
import retrofit2.http.*

interface WorldService {

    @POST("/XYRALITY/WebObjects/BKLoginServer.woa/wa/worlds")
    @Headers("Content-Type: application/json", "Accept: application/json" )
    @FormUrlEncoded
    fun getWorlds(
            @Field("login") login: String,
            @Field("password") password: CharSequence,
            @Field("deviceType") deviceType: String,
            @Field("deviceId") deviceId: String
//            @Body worldPost: WorldPost
    ): Observable<WorldResponseEntity>
}