package com.batynchuk.xyrality.data

import com.batynchuk.xyrality.data.entity.world.WorldResponseEntity
import io.reactivex.Observable

interface BaseWorldRepository {
    fun getWorlds(login: String,
                  password: CharSequence,
                  deviceType: String,
                  deviceId: String): Observable<WorldResponseEntity>
}