package com.batynchuk.xyrality.data.entity.login

data class LoginEntity(val login: String, val password: CharSequence)