package com.batynchuk.xyrality.data.remote.login

import com.batynchuk.xyrality.data.BaseLoginRepository
import com.batynchuk.xyrality.data.entity.login.LoginEntity
import io.reactivex.Observable

class RemoteLoginRepository : BaseLoginRepository {
    override fun login(login: String, password: CharSequence): Observable<LoginEntity> {
        // verify creds here
        return Observable.just(LoginEntity(login, password))
    }

}