package com.batynchuk.xyrality.data.entity.world

data class WorldResponseEntity(val serverVersion: String, val allAvailableWorlds: List<WorldEntity>)