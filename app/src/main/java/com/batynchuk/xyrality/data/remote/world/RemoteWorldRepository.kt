package com.batynchuk.xyrality.data.remote.world

import com.batynchuk.xyrality.data.entity.world.WorldResponseEntity
import com.batynchuk.xyrality.data.remote.BaseRemoteRepository
import io.reactivex.Observable

class RemoteWorldRepository(private val worldService: WorldService) : BaseRemoteRepository {

    override fun getWorlds(login: String,
                           password: CharSequence,
                           deviceType: String,
                           deviceId: String): Observable<WorldResponseEntity> =
            worldService.getWorlds(login, password, deviceType, deviceId)


}