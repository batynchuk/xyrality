package com.batynchuk.xyrality.data.entity.world

import com.google.gson.annotations.SerializedName

data class WorldEntity(var id: Long?,
                       var language: String?,
                       var url: String?,
                       var country: String?,
                       @SerializedName("worldStatus")
                       var worldStatusEntity: WorldStatusEntity?,
                       var mapUrl: String?,
                       var name: String?)