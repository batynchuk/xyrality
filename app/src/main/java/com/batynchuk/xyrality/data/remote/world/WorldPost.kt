package com.batynchuk.xyrality.data.remote.world

data class WorldPost(val login: String,val password: String,  val deviceType: String, val deviceId: String)