package com.batynchuk.xyrality.data.entity.world

data class WorldStatusEntity(var id: Int,
                             var description: String)