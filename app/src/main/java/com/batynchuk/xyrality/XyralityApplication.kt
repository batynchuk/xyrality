package com.batynchuk.xyrality

import android.app.Application
import com.batynchuk.xyrality.di.ApplicationModule
import com.batynchuk.xyrality.di.DaggerApplicationComponent

class XyralityApplication : Application() {

    val component by lazy {
        DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }
}