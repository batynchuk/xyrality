package com.batynchuk.xyrality.common.base

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<T : MVPView> : ViewModel(), MVPPresenter<T> {

    protected var mvpView : T? = null

    protected val disposables = CompositeDisposable()

    override fun onAttachView(mvpView: T) {
        this.mvpView = mvpView
    }

    override fun onDetachView() {
        mvpView = null
        disposables.clear()
    }

    fun isViewAttached() = mvpView == null
}