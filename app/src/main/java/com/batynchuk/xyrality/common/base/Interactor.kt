package com.batynchuk.xyrality.common.base

interface Interactor {
    fun onDetach()
}
