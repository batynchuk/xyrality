package com.batynchuk.xyrality.common.base

import io.reactivex.disposables.CompositeDisposable

abstract class BaseInteractor : Interactor {

    protected val disposables = CompositeDisposable()

    override fun onDetach() = disposables.clear()

}