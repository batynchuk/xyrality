package com.batynchuk.xyrality.common.base

interface MVPPresenter<in T : MVPView> {

    fun onAttachView(mvpView: T)

    fun onDetachView()

}