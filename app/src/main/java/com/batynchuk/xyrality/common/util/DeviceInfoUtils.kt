package com.batynchuk.xyrality.common.util

import android.annotation.SuppressLint
import java.net.NetworkInterface
import java.util.*
import android.os.Build


@SuppressLint("NewApi")
fun getMACAddress(interfaceName: String?): String {
    try {
        val interfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
        for (intf in interfaces) {
            if (interfaceName != null) {
                if (!intf.name.equals(interfaceName, true))
                    continue
            }
            val mac = intf.hardwareAddress ?: return ""
            val buf = StringBuilder()
            for (idx in mac.indices)
                buf.append(String.format("%02X:", mac[idx]))
            if (buf.isNotEmpty())
                buf.deleteCharAt(buf.length - 1)
            return buf.toString()
        }
    } catch (ex: Exception) {
        return "123"
    }
    return "123"
}

