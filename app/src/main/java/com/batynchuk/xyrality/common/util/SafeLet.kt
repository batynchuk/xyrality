package com.batynchuk.xyrality.common.util

fun <T1 : Any, T2 : Any> safeLet(p1: T1?, p2: T2?, block: (T1, T2) -> Unit) {
    return if (p1 != null && p2 != null) block(p1, p2) else {
    }
}